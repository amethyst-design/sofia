<?php
/**
 * The template used for displaying X in the scaffolding library.
 *
 * @package Sofia
 */

?>

<section class="section-scaffolding">
	<h2 class="scaffolding-heading"><?php esc_html_e( 'X', 'sofia' ); ?></h2>
</section>
